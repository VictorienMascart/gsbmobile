package com.victorien.gestionagenda;



import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;



public class MesLieux extends ActionBarActivity {
	
	DatabaseHelper myDb;
	ArrayList<String> listItem;
	ArrayAdapter adapter;
	ListView userlist;

	@Override
	protected void onCreate(Bundle saveIntanceState){
		super.onCreate(saveIntanceState);
		setContentView(R.layout.activity_mes_lieux_visites);
		
		myDb = new DatabaseHelper(this);
		
		listItem = new ArrayList<String>();
		ViewAll();
		userlist = (ListView) findViewById(R.id.listLieuxVisites);
		
		userlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l){
			
				HashMap Lieu = (HashMap) userlist.getItemAtPosition(i);
				
				String myID = (String) Lieu.get("id");
				
				int ID = Integer.parseInt(myID);
				
				MessageClick("D�tails",ID);
			}
		});
	}
	
	@Override
	public void onResume(){
		super.onResume();
		ViewAll();
	}
	
	public void ViewAll(){
		
		ArrayList<HashMap<String, String>> userList = myDb.GetLieuxVisites();
        ListView lv = (ListView) findViewById(R.id.listLieuxVisites);
        ListAdapter adapter = new SimpleAdapter(MesLieux.this, userList, R.layout.list_row,new String[]{"nom","prenom","ville", "id"}, new int[]{R.id.nomC, R.id.prenomC, R.id.villeC});
        lv.setAdapter(adapter);
		
	}
	
	public int LeID;
	
	public void MessageClick(String titre,int ID){
		
		 LeID = ID;
		
		AlertDialog alertDialog;
	
	    alertDialog = new AlertDialog.Builder(this).create();

	    alertDialog.setTitle(titre);

	    ArrayList<HashMap<String, String>> userList = myDb.GetLieuById(ID);
	    
	    HashMap Lieu = (HashMap) userList.get(0);
	    
	    alertDialog.setMessage("Nom : " + (String) Lieu.get("nom") +"\n"+
	    					   "Prenom : " + (String) Lieu.get("prenom")+"\n"+
	    					   "Adresse : " + (String) Lieu.get("rue")+"\n"+
	    					   (String) Lieu.get("ville")+" "+(String)Lieu.get("cp")+"\n"+
	    					   "T�l�phone : "+ (String) Lieu.get("telephone"));
	    
	   
	    alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
	    	
	      public void onClick(DialogInterface dialog, int id) {
	    	  //OK
	    	}
	    });
	    alertDialog.show();
	}
}
