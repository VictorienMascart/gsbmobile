package com.victorien.gestionagenda;

public class Lieux {
	
	private int ID;
	private String Nom; 
	private String Prenom;
	private String Rue;
	private String Cp;
	private String Ville;
	private String Telephone;
	
	public Lieux(int id, String nom, String prenom, String rue, String cp, String ville, String telephone){
		
		ID = id;
		Nom = nom; 
		Prenom = prenom;
		Rue = rue;
		Cp = cp;
		Ville = ville;
		Telephone = telephone;
		
	}
	public Lieux(String nom, String prenom, String ville, int id){
		
		ID =id;
		Nom = nom; 
		Prenom = prenom;
		Ville = ville;
		
	}
	
	public int getId(){return ID;}
	public String getNom(){return Nom;}
	public String getPrenom(){return Prenom;}
	public String getRue(){return Rue;}
	public String getCp(){return Cp;}
	public String getVille(){return Ville;}
	public String getTelephone(){return Telephone;}

}
