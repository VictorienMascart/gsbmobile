package com.victorien.gestionagenda;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ModifierLieu extends Activity {
	
	private int LeID;
	
	EditText TxtNom, TxtPrenom, TxtRue, TxtVille, TxtCp, TxtTelephone;
	Button btnModifier;
	
	DatabaseHelper myDb;
	
	@Override
	protected void onCreate(Bundle save){
		super.onCreate(save);
		setContentView(R.layout.modifier_lieu);
		
		myDb = new DatabaseHelper(this);
		
		//Intent envoy� avec le param�tre ID
		final Intent intent = getIntent();
		LeID = intent.getIntExtra("id", 0);
		
		TxtNom = (EditText) findViewById(R.id.TxtNom);
		TxtPrenom = (EditText) findViewById(R.id.TxtPrenom);
		TxtRue = (EditText) findViewById(R.id.TxtRue);
		TxtVille = (EditText) findViewById(R.id.TxtVille);
		TxtCp = (EditText) findViewById(R.id.TxtCP);
		TxtTelephone = (EditText) findViewById(R.id.TxtTelephone);
		
		btnModifier= (Button) findViewById(R.id.btnModif);
		AddDataInField(LeID);
		UpdateData();
		
	}
	
	public void AddDataInField(int id){
		
		ArrayList<HashMap<String, String>> userList = myDb.GetLieuById(id);
		
		HashMap Lieu = (HashMap) userList.get(0);
		
		TxtNom.setText((CharSequence) Lieu.get("nom"));
		TxtPrenom.setText((CharSequence) Lieu.get("prenom"));
		TxtRue.setText((CharSequence) Lieu.get("rue"));
		TxtVille.setText((CharSequence) Lieu.get("ville"));
		TxtCp.setText((CharSequence) Lieu.get("cp"));
		TxtTelephone.setText((CharSequence) Lieu.get("telephone"));
	}
	
	public void UpdateData(){
		
		btnModifier.setOnClickListener(
				new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						myDb.UpdateData(LeID, 
										TxtNom.getText().toString() ,
										TxtPrenom.getText().toString() ,
										TxtRue.getText().toString() ,
										TxtCp.getText().toString() ,
										TxtVille.getText().toString() ,
										TxtTelephone.getText().toString()
										);
						Toast.makeText(ModifierLieu.this,"Data updated", Toast.LENGTH_LONG).show();
						finish();
					}
				}
		);
	}
	
}
