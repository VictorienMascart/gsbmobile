package com.victorien.gestionagenda;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "Aganda.db";
	public static final String TABLE_NAME = "Lieux";
	public static final String COL_1 = "ID";
	public static final String COL_2 = "NomC";
	public static final String COL_3 = "PrenomC";
	public static final String COL_4 = "RueC";
	public static final String COL_5 = "CodePostalC";
	public static final String COL_6 = "VilleC";
	public static final String COL_7 = "TelephoneC";
	
	public static final String COL_8 = "VisiteON";
	
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
		SQLiteDatabase db = this.getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table "+ TABLE_NAME +"(ID INTEGER PRIMARY KEY AUTOINCREMENT, NomC TEXT, PrenomC TEXT, RueC TEXT, CodePostalC TEXT, VilleC TEXT, TelephoneC TEXT, VisiteON NUMERIC DEFAULT 0)");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
		onCreate(db);
		
	}
	
	public boolean insertData(String nom, String prenom, String rue, String cp, String ville, String telephone ){
		
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL_2, nom);
		contentValues.put(COL_3, prenom);
		contentValues.put(COL_4, rue);
		contentValues.put(COL_5, cp);
		contentValues.put(COL_6, ville);
		contentValues.put(COL_7, telephone);
		long result = db.insert(TABLE_NAME, null, contentValues);
		if(result == -1)
			return false;
		else
			return true; 
	}
	
public void UpdateData(int id, String nom, String prenom, String rue, String cp, String ville, String telephone ){
		
		SQLiteDatabase db = this.getWritableDatabase();
	
		String sql = "UPDATE " + TABLE_NAME + " SET "
		+ COL_2 +"='"+nom+"', "
		+ COL_3 +"='"+prenom+"', "
		+ COL_4 +"='"+rue+"', "
		+ COL_5 +"='"+cp+"', "
		+ COL_6 +"='"+ville+"', "
		+ COL_7 +"='"+telephone+ "' WHERE "+COL_1+"="+id;
	
		db.execSQL(sql);
	
	}
	
	public void DeleteLieu(int userid){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COL_1+" = ?",new String[]{String.valueOf(userid)});
        db.close();
    }
	
	public Cursor getAllData(){
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor res = db.rawQuery("select * from "+ TABLE_NAME, null);
		return res;
	}
	
	public ArrayList<HashMap<String, String>> GetLieux(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<HashMap<String, String>>();
        String query = "SELECT ID, NomC, PrenomC, VilleC FROM "+ TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        while (cursor.moveToNext()){
            HashMap<String,String> user = new HashMap<String, String>();
            user.put("id",cursor.getString(cursor.getColumnIndex(COL_1)));
            user.put("nom",cursor.getString(cursor.getColumnIndex(COL_2)));
            user.put("prenom",cursor.getString(cursor.getColumnIndex(COL_3)));
            user.put("ville",cursor.getString(cursor.getColumnIndex(COL_6)));
            userList.add(user);
        }
        return  userList;
    }
	
	public ArrayList<HashMap<String, String>> GetLieuById(int userid){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<HashMap<String, String>>();
        String query = "SELECT * FROM "+ TABLE_NAME;
        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_2, COL_3, COL_4, COL_5, COL_6, COL_7}, COL_1+ "=?",new String[]{String.valueOf(userid)},null, null, null, null);
        if (cursor.moveToNext()){
            HashMap<String,String> user = new HashMap<String, String>();
            user.put("nom",cursor.getString(cursor.getColumnIndex(COL_2)));
            user.put("prenom",cursor.getString(cursor.getColumnIndex(COL_3)));
            user.put("rue",cursor.getString(cursor.getColumnIndex(COL_4)));
            user.put("cp",cursor.getString(cursor.getColumnIndex(COL_5)));
            user.put("ville",cursor.getString(cursor.getColumnIndex(COL_6)));
            user.put("telephone",cursor.getString(cursor.getColumnIndex(COL_7)));
            userList.add(user);
        }
        return  userList;
    }
	
	public void setVisiteON(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		
		String sql = "UPDATE " + TABLE_NAME + " SET "+ COL_8 +"=1 WHERE "+COL_1+"="+id;
		
		db.execSQL(sql);
	}
	
	public ArrayList<HashMap<String, String>> GetLieuxVisites(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<HashMap<String, String>>();
        String query = "SELECT ID, NomC, PrenomC, VilleC FROM "+ TABLE_NAME + " WHERE "+COL_8+"=1";
        Cursor cursor = db.rawQuery(query,null);
        while (cursor.moveToNext()){
            HashMap<String,String> user = new HashMap<String, String>();
            user.put("id",cursor.getString(cursor.getColumnIndex(COL_1)));
            user.put("nom",cursor.getString(cursor.getColumnIndex(COL_2)));
            user.put("prenom",cursor.getString(cursor.getColumnIndex(COL_3)));
            user.put("ville",cursor.getString(cursor.getColumnIndex(COL_6)));
            userList.add(user);
        }
        return  userList;
    }

}
