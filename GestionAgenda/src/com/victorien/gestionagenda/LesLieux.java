package com.victorien.gestionagenda;

//import android.app.Activity;
//import android.content.Intent;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData.Item;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
//import android.view.MenuItem;
import android.widget.Button;

public class LesLieux extends ActionBarActivity {
	
	private Button button;
	DatabaseHelper myDb;
	ArrayList<String> listItem;
	ArrayAdapter adapter;
	ListView userlist;

	@Override
	protected void onCreate(Bundle saveIntanceState){
		super.onCreate(saveIntanceState);
		setContentView(R.layout.activity_menu_gestion);
		
		myDb = new DatabaseHelper(this);
		
		listItem = new ArrayList<String>();
		ViewAll();
		
		userlist = (ListView) findViewById(R.id.listLieux);
		
		userlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l){
			
				HashMap Lieu = (HashMap) userlist.getItemAtPosition(i);
				
				String myID = (String) Lieu.get("id");
				
				int ID = Integer.parseInt(myID);
				
				MessageClick("D�tails",ID);
			}
		});
		
		button = (Button) findViewById(R.id.btn_ajout);
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				openActivityAjoutLieu();
			}
		});		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		
		ViewAll();
	}
	
	public void openActivityAjoutLieu(){
		
		Intent intent = new Intent(this, AjoutLieu.class);
		startActivity(intent);
	}
	
	public void ViewAll(){
		
		ArrayList<HashMap<String, String>> userList = myDb.GetLieux();
        ListView lv = (ListView) findViewById(R.id.listLieux);
        ListAdapter adapter = new SimpleAdapter(LesLieux.this, userList, R.layout.list_row,new String[]{"nom","prenom","ville", "id"}, new int[]{R.id.nomC, R.id.prenomC, R.id.villeC});
        lv.setAdapter(adapter);
		
	}
	
	public int LeID;
	
	public void MessageClick(String titre,int ID){
		
		 LeID = ID;
		
		AlertDialog alertDialog;
	
	    alertDialog = new AlertDialog.Builder(this).create();

	    alertDialog.setTitle(titre);

	    ArrayList<HashMap<String, String>> userList = myDb.GetLieuById(ID);
	    
	    HashMap Lieu = (HashMap) userList.get(0);
	    
	    alertDialog.setMessage("Nom : " + (String) Lieu.get("nom") +"\n"+
	    					   "Prenom : " + (String) Lieu.get("prenom")+"\n"+
	    					   "Adresse : " + (String) Lieu.get("rue")+"\n"+
	    					   (String) Lieu.get("ville")+" "+(String)Lieu.get("cp")+"\n"+
	    					   "T�l�phone : "+ (String) Lieu.get("telephone"));
	    
	   
	    alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Visiter", new DialogInterface.OnClickListener() {
	    	
	      public void onClick(DialogInterface dialog, int id) {
	    	  
	    	  myDb.setVisiteON(LeID);
	 
	      }
	      });

	    alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Supprimer", new DialogInterface.OnClickListener() {

	      public void onClick(DialogInterface dialog, int id) {
	    	  
	    	  myDb.DeleteLieu(LeID);
	    	  ViewAll();
	    	  
	    }}); 

	    alertDialog.setButton(Dialog.BUTTON_NEUTRAL, "Modifier", new DialogInterface.OnClickListener() {

	      public void onClick(DialogInterface dialog, int id) {

	    	  Intent intent = new Intent(LesLieux.this, ModifierLieu.class);
	    	  intent.putExtra("id", LeID);
	  		  startActivityForResult(intent, 0);

	    }});
	    
	    alertDialog.show();
	}
}