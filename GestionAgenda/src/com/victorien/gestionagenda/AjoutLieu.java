package com.victorien.gestionagenda;

import android.app.Activity;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AjoutLieu extends Activity {
	
	EditText TxtNom, TxtPrenom, TxtRue, TxtVille, TxtCp, TxtTelephone;
	Button btnAjout;
	
	DatabaseHelper myDb;
	@Override
	protected void onCreate(Bundle save){
		super.onCreate(save);
		setContentView(R.layout.ajout_lieu);
		
		myDb = new DatabaseHelper(this);
		
		TxtNom = (EditText) findViewById(R.id.TxtNom);
		TxtPrenom = (EditText) findViewById(R.id.TxtPrenom);
		TxtRue = (EditText) findViewById(R.id.TxtRue);
		TxtVille = (EditText) findViewById(R.id.TxtVille);
		TxtCp = (EditText) findViewById(R.id.TxtCP);
		TxtTelephone = (EditText) findViewById(R.id.TxtTelephone);
		
		btnAjout = (Button) findViewById(R.id.btnAjout);
		AddData();
		
	}
	
	public void AddData(){
		
		btnAjout.setOnClickListener(
				new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						boolean isInserted = 
										myDb.insertData(TxtNom.getText().toString() ,
										TxtPrenom.getText().toString() ,
										TxtRue.getText().toString() ,
										TxtCp.getText().toString() ,
										TxtVille.getText().toString() ,
										TxtTelephone.getText().toString()
										);
						
						if(isInserted = true){
							Toast.makeText(AjoutLieu.this,"Data Inserted", Toast.LENGTH_LONG).show();
							finish();
						}
						else
							Toast.makeText(AjoutLieu.this,"Data not Inserted", Toast.LENGTH_LONG).show();
					}
				}
		);
	}
}
