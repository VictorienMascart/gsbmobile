package com.wallon.gsb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.mnuGestionLieux:
    		Intent intent = new Intent(MainActivity.this, MenuGestion.class);
        	startActivity(intent);
    		return false;
    	case R.id.mnuMesLieux:
    		Toast.makeText(this, "gestion tache selected", Toast.LENGTH_SHORT).show();
    		return true;
    	case R.id.mnuQuitter:
    		Toast.makeText(this, "fermeture de l'application", Toast.LENGTH_SHORT).show();
    		finish();
    		return true;
    	default: 
    		return super.onOptionsItemSelected(item);
    	}
    	
    }
    
}
